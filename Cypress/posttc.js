/// <reference types="cypress" />


it("create", ()=>{
    cy
    .request({
        method: "POST",
        url: "https://reqres.in/api/users",
        body:{
            "name": "morpheus",
            "job": "leader"
        }
    }).then((res)=>{
        expect(res.status).eq(201)
        expect(res.body).not.be.empty
        console.log(res.body)
        expect(res.body).has.property("job", "leader")
    })
})

it("register", ()=>{

    cy
    .request({
        method:"POST",
        url:"https://reqres.in/api/register",
        body:{
            
             "email": "eve.holt@reqres.in",
             "password": "pistol"
        }
    }).then((res)=>{
        expect(res.status).eq(200)
        expect(res.body).not.be.empty
        console.log(res.body)
    })
})

it.only("login succesfull", ()=>{
    cy
    .request({
        method: "POST",
        url:"https://reqres.in/api/login",
        body:{
         
             "email": "eve.holt@reqres.in",
             "password": "cityslicka"
        }
    }).then((res)=>{
        expect(res.status).eq(200)
        console.log(res.body)
        expect(res.body).has.property("token")
    })
})