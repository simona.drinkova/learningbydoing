/// <reference types="cypress" />

let id=""

it("pocasie zilina", ()=>{

    cy
    .request({
        method:"GET",
        url: "api.openweathermap.org/data/2.5/weather?q=Zilina&appid=5c8b767325b7f50761c0a4c4da79786d",
     
    }).then((res)=>{
        expect(res.status).eq(200)
        expect(res.body).has.property("name", "Zilina")
        id=res.body.id
        console.log(id)
        
        
    })
})

it("predpoved na 5 dni podla ID", ()=>{

    cy
    .request({
        method: "GET",
        url: "api.openweathermap.org/data/2.5/forecast?id=" + id + "&appid=5c8b767325b7f50761c0a4c4da79786d",
            
      

    }).then((res)=>{
        expect(res.status).eq(200)
        cy.log(res.body)
    })


  

})