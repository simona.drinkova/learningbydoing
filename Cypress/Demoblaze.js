/// <reference types="cypress" />


it('demoblaze', function(){

    cy.visit('https://www.demoblaze.com/')
    cy.get('#navbarExample').find('.nav-item:visible').should('have.length', 6)
    cy.get('#nava').should('be.visible')
    
    cy.get('.col-lg-3').find('.list-group').contains('Phones').click()
    cy.wait(2000)
    cy.get('#tbodyid')
    cy.get(':nth-child(1) > .card > .card-block > .card-title > .hrefch').click()
    cy.get('.name').should('have.text', 'Samsung galaxy s6')
    cy.get('.price-container').should('have.text', '$360 *includes tax')
    cy.get('.product-content').contains('Add to cart').click()
    cy.on('window:alert', (str)=>{

        expect(str).to.eq('Product added')
    })
    cy.get('#cartur').click()
    cy.get('.col-lg-1 > .btn').click()
    cy.get('#name').type('Simi')
    cy.get('#country').type('Slovakia')
    cy.get('#city').type('Bratislava')
    
    })
    
it('viacProduktov', function(){

    cy.visit('https://www.demoblaze.com/')
    cy.get('#tbodyid:visible').find('.card-title').should('have.length', 9)
    cy.get('.card-title').contains('Nokia lumia 1520')
    cy.get('.card-title')


    cy.get('#tbodyid:visible').find('.card-title').each(($el, index, $list)=>{

    const phone=  $el.find('.hrefch').text()
        if(phone.includes('Nexus 6')){
           cy.wrap($el).click()
        }
        
    })

})

it.only('signup', function(){

    cy.visit('https://www.demoblaze.com/')
    cy.get('#signin2').click()
    cy.wait(1000)
    cy.get('#sign-username:visible').type('Simi')
    cy.wait(2000)
    cy.get('#sign-password:visible').type('ssddd')
    cy.wait(2000)
    cy.get('.modal-footer:visible').find('.btn-primary').click()
    cy.on('window:alert',(str)=>{

        expect(str).to.eq('This user already exist.')
      
    })
    

})