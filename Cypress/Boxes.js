/// <reference types="cypress" />


beforeEach(()=>{

  cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
})

it('radioButton',function(){

    cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
    cy.get('input[value="radio2"]').check().should('be.checked')

})

it('classExample', function(){

    cy.get('#autocomplete').type('Ind').wait(2000)
    cy.get('.ui-menu-item').each(($el, index, $list)=>{

        if($el.text()=== 'India'){

            $el.click()
        }

    })

})

it('DropDown',function(){

    cy.get('select').select('option2').should('have.value', 'option2')

})

it('Checkbox', function(){

    cy.get('#checkBoxOption1').check().should('be.checked')
    cy.get('#checkBoxOption1').uncheck().should('not.be.checked')
})

it('switchWin.', function(){

    cy.get('')

})

it('SwitchTab.', function(){

    cy.get('#opentab').invoke('removeAttr', 'target').click()
    cy.url().should('include', 'rahulshettyacademy.com')

})

it('Alert.', function(){

    cy.get('#name')
    cy.get('#alertbtn').click()
    cy.on('window:alert', (str)=>{

        expect(str).to.eq('Hello , share this practice page and share your knowledge')
    })    

})

it('DisplayedElement.', function(){

    cy.get('#displayed-text').should('be.visible')
    cy.get('#hide-textbox').click()
    cy.get('#displayed-text').should('not.be.visible')
    cy.get('#show-textbox').click()
    cy.get('#displayed-text').should('be.visible')


})

it('WebTableEx',function(){

    cy.get('#product')
    cy.get('fieldset > #product > tbody > :nth-child(2) > :nth-child(2)')
    
})

it.only('WebTableFixer', function(){

    cy.get('.tableFixHead').find('#product')
    cy.get('#product').find('.thead > tr > :nth-child(1)')

})