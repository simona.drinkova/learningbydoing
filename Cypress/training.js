/// <reference types="cypress" />



it('add to cart', function(){

    cy.visit('https://rahulshettyacademy.com/seleniumPractise/#/')
    cy.get('.search-keyword').type('po')
    cy.wait(2000)

    cy.get('.products > :nth-child(1)').contains('ADD TO CART').click()
    cy.get('.cart-icon > img').click()
    cy.get('div.container header:nth-child(1) div.container div.cart div.cart-preview.active:nth-child(6) > div.action-block:nth-child(2)').click()
    cy.get(':nth-child(14)').click()

    cy.get('select').select('Slovakia')
    cy.get('.chkAgree').click()
    cy.get('button').click()
 
})

it('verifired', function(){

    cy.visit('https://rahulshettyacademy.com/seleniumPractise/#/')
    cy.get('.search-keyword').type('po')
    cy.wait(2000)
    cy.get('.product:visible').should('have.length', 2)

    cy.get('.products').find('.product').eq(1)

})

it('checkboxes', function(){

    cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
    cy.get('[value="radio3"]').check().should('be.checked')
    cy.get('select').select('option2').should('have.value', 'option2')//static
    cy.get('input[type="checkbox"]').check(['option1', 'option3']).should('be.checked')
    cy.get('#autocomplete').type('Ind')
    cy.get('.ui-menu-item div').each(($el, index, $list)=>{

        if($el.text()=== 'India'){
            $el.click()
        } //dynamic

    })


})

it('tableButt', function(){

    cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
    cy.get('#opentab').invoke('removeAttr', 'target').click()
    cy.url().should('include', 'rahulshettyacademy.com')
})

it('alerts', function(){

    cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
    cy.get('#name').type('Simi')
    cy.get('#alertbtn').click()
    cy.get('#confirmbtn').click()
    cy.on('window:alert',(str)=>{

        expect(str).to.eq('Hello Simi, share this practice page and share your knowledge')

    })
    
})

it('elementdispl', function(){

    cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
    cy.get('#displayed-text').should('be.visible')
    cy.get('#hide-textbox').click()
    cy.get('#displayed-text').should('not.be.visible')
    cy.get('#show-textbox').click()
    cy.get('#displayed-text').should('be.visible')

})

it('apis', function(){

    cy.request({
        method: 'GET',
        url:'http://216.10.245.166/Library/GetBook.php?AuthorName=somename',


    }).then((res)=>{
        expect(res.status).eq(200)
        expect(res.body).not.be.empty
        cy.log(res.body[1].book_name)
        expect(res.body[1].book_name).include('Learn Automation with JavaScript')
        
        
    })


})
it.only('postik', function(){

    cy.request({
        method:"POST",
        url:"https://rahulshettyacademy.com/maps/api/place/add/json?key=qaclick123",
        body:{
            "location": {
              "lat": -38.383494,
              "lng": 33.427362
            },
            "accuracy": 50,
            "name": "Frontline house",
            "phone_number": "(+91) 983 893 3937",
            "address": "29, side layout, cohen 09",
            "types": [
              "shoe park",
              "shop"
            ],
            "website": "http://google.com",
            "language": "French-IN"
          }
          
    }).then((res)=>{
        expect(res.status).eq(200)
        cy.log(res.body)
        expect(res.body).not.be.empty
        
        
        
    })
    
        



})