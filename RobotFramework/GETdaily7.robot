*** Settings ***
Library     RequestsLibrary
Library     Collections

Test Setup    log to console      Opening Browser
Test Teardown  log to console        Closing Browser

*** Variables ***
${token}=       5c8b767325b7f50761c0a4c4da79786d
${lat}=         48.16
${lon}=         17.28
${exclude}=        minutely,hourly,current,national,historical

*** Test Case ***
Get Request Daily Weather

    create session      dailyweather        https://api.openweathermap.org
    ${params}       Create Dictionary       appid=${token}  lat=${lat}  lon=${lon}  part=${exclude}
    ${response}         GET on session       dailyweather    /data/2.5/onecall   params=${params}


    log to console  ${response.json()}[daily]



    #VALIDATIONS
    ${status_code}=      convert to string      ${response.status_code}
    should be equal     ${status_code}      200
    should be equal     ${lat}          48.16
    should be equal     ${lon}          17.28










