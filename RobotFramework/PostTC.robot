*** Settings ***
Library     SeleniumLibrary
Library     RequestsLibrary
Library     Collections

*** Variables ***


*** Test Cases ***
POST UnRegister

        create session      unregister        https://reqres.in
        ${body}=    create dictionary       email=sydney@fife
        ${header}=  create dictionary       Content-Type=application/json
        ${resp}=    post request   unregister    /api/register        data=${body}   headers=${header}

        log to console      ${resp.content}
        log to console      ${resp.status_code}


        #VALIDATIONS
        ${status_code}  convert to string   ${resp.status_code}
        Should be equal     ${status_code}      400
        Should not be empty     ${body}
